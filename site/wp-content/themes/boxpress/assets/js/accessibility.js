(function ($) {
  'use strict';

  /**
   * Accessibility Enhancements
   * ===
   */


  /**
   * Aria Nav Controls
   * ---
   * Remove or exclude the 'js-accessible-menu' class if the
   * nav does not have sub-items.
   */

  var $aria_nav = $('.js-accessible-menu');

  $aria_nav.each(function () {
    $(this).wpAriaNav({
      overlay: false
    });
  });



  /**
   * Force all article external links to open in new tabs
   * ---
   * Applies only to anchor tags that don't already
   * have a target attribute
   */

  // Ignore article links that already have a target set,
  // link to an anchor, or link to a relative URL
  var $page_links = $( '.entry-content, .page-content' ).find( 'a:not([target]):not([href^="#"]):not([href^="/"])' );

  // Get the site's base URL
  var pathArray = location.href.split( '/' );
  var host      = pathArray[2]; // ex. 'www.imagebox.com', 'domain.com'

  $page_links.each(function () {
    var $this = $(this);
    var this_href = $this.attr('href');
    var this_rel  = $this.attr('rel');

    if ( typeof this_href !== typeof undefined && this_href !== false ) {
      // Check if link is external
      if ( this_href.indexOf( host ) === -1 ) {
        $this.attr( 'target', '_blank' );

        // Check if rel is already set
        if ( typeof this_rel !== typeof undefined && this_rel !== false ) {
          $this.attr( 'rel', 'nofollow noopener' );
        }
      }
    }
  });


  /**
   * Add `focusable="false"` to SVGs inside links or buttons
   */

  var $focusable_svgs = $('a svg, button svg');

  $focusable_svgs.each(function () {
    var $this = $(this);
    var this_focusable = $this.attr('focusable');

    if ( typeof this_focusable !== typeof undefined && this_focusable !== false ) {
      $this.attr( 'focusable', 'false' );
    }
  });

})(jQuery);

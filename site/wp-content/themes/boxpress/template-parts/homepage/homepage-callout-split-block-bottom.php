<?php
/**
 * Displays the slipt content / image layout
 *
 * @package boxpress
 */

$callout_split_header_bottom    = get_field( 'callout_split_header_bottom' );
$callout_split_text_bottom    = get_field( 'callout_split_text_bottom' );
$callout_split_link_bottom    = get_field( 'callout_split_link_bottom' );

$callout_split_image_bottom      = get_field( 'callout_split_image_bottom' );
?>
<section class="split-block-layout callout-split-image-bottom--section">
  <div class="split-block-col">

    <?php if ( $callout_split_image_bottom ) : ?>

      <img class="split-block-image"
        src="<?php echo esc_url( $callout_split_image_bottom['url'] ); ?>"
        width="<?php echo esc_attr( $callout_split_image_bottom['width'] ); ?>"
        height="<?php echo esc_attr( $callout_split_image_bottom['height'] ); ?>"
        alt="<?php echo esc_attr( $callout_split_image_bottom['alt'] ); ?>">

    <?php endif; ?>

  </div>
  <div class="split-block-col">
    <div class="split-block-content">
      <div class="split-block-content-inner-wrap">
        <div class="callout-text">
          <h2><?php echo $callout_split_header_bottom; ?></h2>
          <p><?php echo $callout_split_text_bottom; ?></p>
          <?php if ( $callout_split_link_bottom ) : ?>
            <?php
              $callout_split_link_bottom_target = ! empty( $callout_split_link_bottom['target'] ) ? $callout_split_link_bottom['target'] : '_self';
            ?>
            <a class="button-white-bg"
              href="<?php echo esc_url( $callout_split_link_bottom['url'] ); ?>"
              target="<?php echo esc_attr( $callout_split_link_bottom_target ); ?>">
              <?php echo $callout_split_link_bottom['title']; ?>
            </a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section blog--section">
  <div class="wrap">

    <?php
    $blog_header  = get_field( 'blog_header' );
    $blog_text   = get_field( 'blog_text' );
    $blog_link   = get_field( 'blog_link' );
    ?>
    <div class="blog-text">
      <h2><?php echo $blog_header; ?></h2>
      <p><?php echo $blog_text; ?></p>
      <?php if ( $blog_link ) : ?>
        <?php
          $blog_link_target = ! empty( $blog_link['target'] ) ? $blog_link['target'] : '_self';
        ?>
        <a class="button-white-bg"
          href="<?php echo esc_url( $blog_link['url'] ); ?>"
          target="<?php echo esc_attr( $blog_link_target ); ?>">
          <?php echo $blog_link['title']; ?>
        </a>
      <?php endif; ?>
      </div>

     <div class="blog-posts">
         <?php // Latest Posts Example ?>
         <?php
           $home_post_query_args = array(
             'post_type' => 'post',
             'posts_per_page' => 3
           );
           $home_post_query = new WP_Query( $home_post_query_args );
         ?>
         <?php if ( $home_post_query->have_posts() ) : ?>

               <div class="l-grid l-grid--three-col">

                 <?php while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>

                   <div class="l-grid-item">
                     <div>
                       <a href="<?php the_permalink(); ?>">
                         <div class="card-thumb">
                           <?php the_post_thumbnail();?>
                         </div>
                         <div class="card-body">
                           <div class="header">
                             <p class="date"><?php the_date(); ?></p>
                             <h3><?php the_title(); ?></h3>
                           </div>
                           <div class="button-white-bg">
                             <?php _e('Learn More', 'boxpress'); ?>
                           </div>
                         </div>
                       </a>
                     </div>
                   </div>

                 <?php endwhile; ?>
               </div>
               <?php wp_reset_postdata(); ?>
             <?php endif; ?>
       </div>

        </div>
      </section>

<?php
/**
 * Displays the slipt content / image layout
 *
 * @package boxpress
 */


// left
$callout_split_header_top    = get_field( 'callout_split_header_top' );
$callout_split_text_top    = get_field( 'callout_split_text_top' );
$callout_split_link_top    = get_field( 'callout_split_link_top' );
// right
$callout_split_image_top      = get_field( 'callout_split_image_top' );

?>

<section class="section callout-split-image-top--section">
  <div class="wrap">
    <div class="l-grid l-grid--two-col">
      <div class="l-grid-item">
        <div class="callout-text">
          <h2><?php echo $callout_split_header_top; ?></h2>
          <p><?php echo $callout_split_text_top; ?></p>
          <?php if ( $callout_split_link_top ) : ?>
            <?php
              $callout_split_link_top_target = ! empty( $callout_split_link_top['target'] ) ? $callout_split_link_top['target'] : '_self';
            ?>
            <a class="button-white-bg"
              href="<?php echo esc_url( $callout_split_link_top['url'] ); ?>"
              target="<?php echo esc_attr( $callout_split_link_top_target ); ?>">
              <?php echo $callout_split_link_top['title']; ?>
            </a>
          <?php endif; ?>
        </div>
      </div>
      <div class="l-grid-item">
       <div class="callout-photo">
         <?php if ( $callout_split_image_top ) : ?>
           <img
             src="<?php echo esc_url( $callout_split_image_top['url'] ); ?>"
             width="<?php echo esc_attr( $callout_split_image_top['width'] ); ?>"
             height="<?php echo esc_attr( $callout_split_image_top['height'] ); ?>"
             alt="<?php echo esc_attr( $callout_split_image_top['alt'] ); ?>">
         <?php endif; ?>
       </div>
      </div>
    </div>
  </div>
</section>

<?php
/**
 * Displays the Hero layout
 *
 * Easily convertible into a sideshow by enabling multiple
 * rows in the repeater.
 *
 * @package boxpress
 */
?>
<?php if ( have_rows( 'homepage_hero' )) : ?>
  <?php while ( have_rows( 'homepage_hero' )) : the_row();
      $header_content_left_one     = get_sub_field( 'header_content_left_one' );
      $header_content_left_two     = get_sub_field( 'header_content_left_two' );

      $header_content_right_two     = get_sub_field( 'header_content_right_two' );
      $copy_content_right_two     = get_sub_field( 'copy_content_right_two' );
      $hero_link        = get_sub_field( 'hero_link' );

      $hero_background  = get_sub_field( 'hero_background' );
      $hero_background_image = get_sub_field( 'hero_background_image' );
    ?>

    <section class="hero <?php echo $hero_background; ?>">
      <div class="wrap">
        <div class="hero-box">
          <div class="hero-content">

            <div class="hero-content--left">
              <!-- <?php // if ( $hero_link ) : ?>
                <?php
                //  $hero_link_target = ! empty( $hero_link['target'] ) ? $hero_link['target'] : '_self';
                ?>
                <a class="button button--reverse"
                  href="<?php // echo esc_url( $hero_link['url'] ); ?>"
                  target="<?php // echo esc_attr( $hero_link_target ); ?>">
                  <?php // echo $hero_link['title']; ?>
                </a>
              <?php // endif; ?> -->
              <div class="left-headers">
                <h1><?php echo $header_content_left_one; ?></h1>
                <h1><?php echo $header_content_left_two; ?></h1>
              </div>
              <div class="box">
                  <h1><?php echo $header_content_right_two; ?></h1>
              </div>
            </div>

            <div class="hero-content--right">
              <p><?php echo $copy_content_right_two; ?></p>
              <?php if ( $hero_link ) : ?>
                <?php
                  $hero_link_target = ! empty( $hero_link['target'] ) ? $hero_link['target'] : '_self';
                ?>
                <a class="button button--reverse"
                  href="<?php echo esc_url( $hero_link['url'] ); ?>"
                  target="<?php echo esc_attr( $hero_link_target ); ?>">
                  <?php echo $hero_link['title']; ?>
                </a>
              <?php endif; ?>
            </div>
            </div>
          </div>
          <div class="hero-message hide-for-mobile">
            <span>
              Serving small businesses within a 2 hour radius of Pittsburgh, including Cleveland.
            </span>
          </div>
        </div>
        <div class="box">
          <div class="hero-message hide-for-desktop">
            <span>
              Serving small businesses within a 2 hour radius of Pittsburgh, including Cleveland.
            </span>
          </div>
        </div>
      </section>

  <?php endwhile; ?>
<?php endif; ?>

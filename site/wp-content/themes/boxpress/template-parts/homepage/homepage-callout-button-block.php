<?php
/**
 * Displays the slipt content / image layout
 *
 * @package boxpress
 */


// Center
$callout_button_block_header    = get_field( 'callout_button_block_header' );
$callout_button_block_link    = get_field( 'callout_button_block_link' );

?>

<section class="section callout--section">
  <div class="wrap">
    <div class="button-block">
      <div class="callout-header">
         <h2><?php echo $callout_button_block_header; ?></h2>
      </div>
      <div class="callout-body">
        <div class="button-callout-content">
          <?php if ( $callout_button_block_link ) : ?>
            <?php
              $callout_button_block_link_target = ! empty( $callout_button_block_link['target'] ) ? $callout_button_block_link['target'] : '_self';
            ?>
            <a class="button-white"
              href="<?php echo esc_url( $callout_button_block_link['url'] ); ?>"
              target="<?php echo esc_attr( $callout_button_block_link_target ); ?>">
              <?php echo $callout_button_block_link['title']; ?>
            </a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>

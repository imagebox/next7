<?php
/**
 * Displays the page banner
 *
 * @package boxpress
 */

$banner_title     = get_the_title();
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );
$photo_banner   = get_field( 'photo_banner' );

if ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

// Set top page title and banner image
if ( 0 !== $post->post_parent ) {
  $post_ancestors = get_post_ancestors( $post->ID );
  $post_id = end( $post_ancestors );
  $banner_title = get_the_title( $post_id );
  $top_featured_image = get_the_post_thumbnail_url( $post_id );

  if ( ! empty( $top_featured_image )) {
    $banner_image_url = $top_featured_image;
  }
}

if ( has_post_thumbnail() ) {
  $banner_image_url = get_the_post_thumbnail_url( get_the_ID() );
}

?>

<header class="banner">
  <div class="wrap">
    <div class="banner-title">
      <?php
        $media_headline = get_field( 'media_headline', get_the_ID() );
        $banner_title = ( ! empty( $media_headline )) ? $media_headline : get_the_title( get_the_ID() );
      ?>
      <span class="h1">
          <?php echo $banner_title; ?>
      </span>
      <?php
        if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb( '<nav class="breadcrumbs" aria-label="Breadcrumb">', '</nav>' );
        }
      ?>
    </div>


    <?php if ( $banner_image_url ) : ?>
      <div class="banner-pic">
          <img class="banner-graphic" draggable="false" src="<?php echo $banner_image_url; ?>" alt="">
      </div>
    <?php else : ?>
      <div class="banner-pic banner-pic-alt">
        <div class="pic-container">
            <img class="banner-graphic" draggable="false" src="<?php echo $photo_banner; ?>" alt="">
        </div>
      </div>
    <?php endif; ?>

  </div>
</header>

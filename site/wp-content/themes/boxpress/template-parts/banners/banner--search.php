<?php
/**
 * Displays the 404 page banner
 *
 * @package boxpress
 */

$banner_title     = __( 'Search', 'boxpress' );
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );
$search_banner_image = get_field( 'search_banner_image', 'option' );

if ( $search_banner_image ) {
  $banner_image_url = $search_banner_image['url'];

} elseif ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

?>
<header class="banner">
  <div class="wrap">
    <div class="banner-title">
      <span class="h1">
        <?php echo $banner_title; ?>
      </span>
      <?php
        if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb( '<nav class="breadcrumbs" aria-label="Breadcrumb">', '</nav>' );
        }
      ?>
    </div>
    <div class="banner-pic">
      <?php if ( ! empty( $banner_image_url )) : ?>
        <img class="banner-graphic" draggable="false" src="<?php echo $banner_image_url; ?>" alt="">
      <?php endif; ?>
    </div>
  </div>
</header>

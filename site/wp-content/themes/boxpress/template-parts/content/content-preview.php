<?php
/**
 * Displays the preview content block
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>

<?php
$categories = get_the_category();
 ?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--preview' ); ?>>
    <a href="<?php the_permalink(); ?>">
      <div class="card-thumb">
        <?php the_post_thumbnail();?>
      </div>
      <div class="card-body">
        <div class="card-header">
          <p class="date"><?php the_date(); ?></p>
          <h2><?php the_title() ?></h2>
          <p><?php the_excerpt() ?></p>
        </div>
        <div class="button-white-bg">
          <?php _e('Learn More', 'boxpress'); ?>
        </div>
      </div>
    </a>
</article>

<?php
/**
 * The template part for displaying results in search pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>

<?php
/**
 * Displays the page banner
 *
 * @package boxpress
 */

$banner_title     = get_the_title();
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );
$photo_banner   = get_field( 'photo_banner' );

if ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

// Set top page title and banner image
if ( 0 !== $post->post_parent ) {
  $post_ancestors = get_post_ancestors( $post->ID );
  $post_id = end( $post_ancestors );
  $banner_title = get_the_title( $post_id );
  $top_featured_image = get_the_post_thumbnail_url( $post_id );

  if ( ! empty( $top_featured_image )) {
    $banner_image_url = $top_featured_image;
  }
}

if ( has_post_thumbnail() ) {
  $banner_image_url = get_the_post_thumbnail_url( get_the_ID() );
}

?>

<?php
$categories = get_the_category();
 ?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--search' ); ?>>
    <a href="<?php the_permalink(); ?>">


     <?php if ( has_post_thumbnail() ) : ?>
        <div class="card-thumb search-page-card-thumb">
          <?php the_post_thumbnail();?>
        </div>
      <?php endif; ?>

      <?php if ( $banner_image_url ) : ?>
        <div class="card-thumb search-page-card-banner">
            <img class="banner-graphic" draggable="false" src="<?php echo $banner_image_url; ?>" alt="">
        </div>
      <?php endif; ?>



      <div class="card-body">
        <div class="header">
          <p class="date"><?php the_date(); ?></p>

          <?php foreach ( $categories as $category ) : ?>
            <p class="cat"><?php echo $category->name; ?></p>
          <?php endforeach; ?>

          <h2><?php the_title() ?></h2>
          <p><?php the_excerpt() ?></p>
        </div>

        <div class="button-white-bg">
          <?php _e('Learn More', 'boxpress'); ?>
        </div>
      </div>
    </a>
</article>

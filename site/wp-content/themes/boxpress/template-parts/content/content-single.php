<?php
/**
 * Template part for displaying blog posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>

<?php
$categories = get_the_category();
 ?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--single' ); ?>>
  <header class="entry-header">
    <h1 class="entry-title"><?php the_title(); ?></h1>

    <div class="entry-meta">
      <?php  boxpress_posted_on(); ?>
      <?php // foreach ( $categories as $category ) : ?>
        <!-- <p class="cat"><?php // echo $category->name; ?></p> -->
        <?php // endforeach; ?>
        <!-- <a style="margin-top: 1.2em;" class="button" href="<?php echo esc_url( home_url( '/insights/' )); ?>" rel="providers">
        <span>Back</span>
      </a> -->
    </div>
    <?php if ( has_post_thumbnail() ) : ?>
      <?php the_post_thumbnail('home_index_thumb'); ?>
    <?php endif; ?>

  </header>

  <div class="entry-content">
    <?php the_content(); ?>
  </div>

  <footer class="entry-footer">
    <?php include( get_template_directory() . '/template-parts/social-share.php'); ?>
  </footer>
</article>

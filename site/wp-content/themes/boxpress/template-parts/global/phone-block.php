<?php
/**
 * Site address and contact info
 *
 * Note - No heading tags allowed in `address` tag :(
 *
 * @package boxpress
 */

$organization_type = get_field( 'organization_type', 'option' );

if ( empty( $organization_type )) {
  $organization_type = 'Organization';
}

?>
<?php if ( have_rows( 'site_offices', 'option' )) : ?>


    <?php
      $company_name     = get_bloginfo( 'name', 'display' );
      $alt_company_name = get_field( 'alternative_company_name', 'option' );

      if ( ! empty( $alt_company_name )) {
        $company_name = $alt_company_name;
      }
    ?>

    <?php while ( have_rows( 'site_offices', 'option' )) : the_row(); ?>
      <?php
        $phone_number   = get_sub_field('phone_number');

        if ( ! empty( $address_line_2 )) {
          $full_street_address = "{$street_address}, {$address_line_2}";
        }
      ?>
      <?php if ( ! empty( $phone_number ) || ! empty( $fax_number ) || ! empty( $email )) : ?>

          <?php if ( ! empty( $phone_number )) : ?>
            <?php
              // Strip hyphens & parenthesis for tel link
              $tel_formatted = str_replace([ ".", "-", "–", "(", ")", " " ], '', $phone_number );
            ?>
              <span class="vh"><?php _e( 'Phone:', 'boxpress' ); ?></span>
              <a href="tel:+1<?php echo $tel_formatted; ?>">
                <span itemprop="telephone"><?php echo $phone_number; ?></span>
              </a>
          <?php endif; ?>

      <?php endif; ?>
    <?php endwhile; ?>


<?php endif; ?>

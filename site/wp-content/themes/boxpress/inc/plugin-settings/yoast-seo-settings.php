<?php
/**
 * Yoast SEO
 *
 * @package boxpress
 */

if ( is_plugin_active( 'wordpress-seo/wp-seo.php' ) || is_plugin_active( 'wordpress-seo-premium/wp-seo-premium.php' )) {

  // Disabled enhanced slack sharing
  add_filter('wpseo_enhanced_slack_data', '__return_false' );
}

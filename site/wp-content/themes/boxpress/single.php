<?php
/**
 * The template for displaying all single posts.
 *
 * @package boxpress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--blog.php'); ?>

  <section class="section blog-page">
    <div class="wrap wrap--limited">
        <div class="l-main-col">

          <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'template-parts/content/content', 'single' ); ?>

            <?php
              // Load Comment Block
              if ( comments_open() || get_comments_number() ) :
                comments_template();
              endif;
            ?>

          <?php endwhile; ?>

        </div>
  </section>

<?php get_footer(); ?>

<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php // Hero ?>
    <?php get_template_part( 'template-parts/homepage/homepage-hero' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-callout-split-block-top' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-callout-button-block' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-blog' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-callout-split-block-bottom' ); ?>


  </article>

<?php get_footer(); ?>

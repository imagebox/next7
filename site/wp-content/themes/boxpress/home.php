<?php
/**
 * The template for displaying the Blog page.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package boxpress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--blog.php'); ?>

  <section class="section blog-page">
    <div class="wrap wrap--limited">

        <div class="l-main-col">
          <header class="page-header vh">
            <h1 class="page-title"><?php echo get_the_title( get_option( 'page_for_posts', true )); ?></h1>
          </header>

          <?php if ( have_posts() ) : ?>


            <?php while ( have_posts() ) : the_post(); ?>


              <?php get_template_part( 'template-parts/content/content-preview' ); ?>

            <?php endwhile; ?>




            <?php boxpress_pagination(); ?>
          <?php else : ?>

            <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

          <?php endif; ?>

        </div>
    </div>
  </section>

<?php get_footer(); ?>

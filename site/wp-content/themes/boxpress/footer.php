<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package boxpress
 */
?>
</main>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="wrap">
    <div class="primary-footer">
      <div class="l-footer l-footer--3-cols l-footer--gap-large">
        <div class="l-footer-item flex-box-left">
          <div class="site-branding">
            <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
              <span class="vh"><?php bloginfo('name'); ?></span>
              <svg class="site-logo" width="205" height="65" focusable="false">
                <use href="#site-logo"/>
              </svg>
            </a>
          </div>
          <?php get_template_part( 'template-parts/global/address-block' ); ?>
          <div class="site-copyright">
            <p>
              <small>
                <?php _e('Copyright', 'boxpress'); ?> &copy; <?php echo date('Y'); ?>
                <?php
                  $company_name     = get_bloginfo( 'name', 'display' );
                  $alt_company_name = get_field( 'alternative_company_name', 'option' );

                  if ( ! empty( $alt_company_name )) {
                    $company_name = $alt_company_name;
                  }
                ?>
                Next7 IT. All Rights Reserved.
              </small>
            </p>
          </div>
          <div class="imagebox">
            <p>
              <small>
                <?php _e('Website by', 'boxpress'); ?>
                <a href="https://imagebox.com" target="_blank">
                  <span>Imagebox</span>
                </a>
              </small>
            </p>
          </div>
        </div>
        <div class="l-footer-item flex-box-center">
          <?php // Footer Navigation ?>
          <?php if ( has_nav_menu( 'footer' )) : ?>
              <nav class="navigation--footer"
                aria-label="<?php _e( 'Footer Navigation', 'boxpress' ); ?>"
                role="navigation">
                <ul class="nav-list">
                  <?php
                    wp_nav_menu( array(
                      'theme_location'  => 'footer',
                      'items_wrap'      => '%3$s',
                      'container'       => false,
                      'walker'          => new Aria_Walker_Nav_Menu(),
                    ));
                  ?>
                </ul>
              </nav>
          <?php endif; ?>
        </div>
        <div class="l-footer-item flex-box-right l-footer-item--pull-right">
          <div class="box">
            <div class="form-box">
           <?php echo gravity_form(1, true, false, false, '', true, 1);  ?>
           </div>
             <?php get_template_part( 'template-parts/global/social-nav' ); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="site-info">
      <div class="l-footer l-footer--2-cols l-footer--align-center">
        <div class="l-footer-item">

        </div>
        <div class="l-footer-item l-footer-item--pull-right">

        </div>
      </div>
    </div> -->
  </div>
</footer>
</div>

<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>
